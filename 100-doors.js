function getFinalOpenedDoors(numDoors) {
  let doors = [...Array(numDoors)]
    .map((_, index) => ({ number: index + 1, state: "closed" }));

  for (let increment = 1; increment <= numDoors; increment++) {
    for (let i = increment - 1; i < numDoors; i += increment) {
      doors[i].state = doors[i].state == "closed" ? "open" : "closed";
    }
  }

  return doors
    .filter(obj => obj.state == "open")
    .map(obj => obj.number);
}

module.exports = getFinalOpenedDoors;
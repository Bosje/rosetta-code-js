const getFinalOpenedDoors = require('./100-doors');

test('getFinalOpenedDoors should produce the correct result', () => {
  expect(getFinalOpenedDoors(100)).toStrictEqual([1, 4, 9, 16, 25, 36, 49, 64, 81, 100]);
});